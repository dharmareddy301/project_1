
#new1 = [u,v,x,y,z]
sections = ["a","b","c","d","e"]
week = ["Mon","Tue","Wed","Thu","Fri"]
time= ["8-9","9-10","11-12","12-1"]
n_subjects =5
teach_hours = 3
def sub(week,time,n_subjects):
	k = 1
	new1 = []
	for i in range(len(week)):
		j = 0
		tmp = []
		while j < len(time):
			tmp.append('sub'+str(k%6))
			j += 1
			k += 1
			if k > n_subjects :
				k = 1

		if k > n_subjects:
			k = 1
		new1.append(tmp)
	return new1	


def section_time(new1,sections,week,time):
	section_tt = {}
	for i in range(len(sections)):
		if i!=0:
			new1 = (new1[-1:] + new1[:-1])	
		for j in range(len(week)):
			for k in range(len(time)):
				if sections[i] not in section_tt:
					section_tt[sections[i]] = {}
				if week[j] not in section_tt[sections[i]]:
					section_tt[sections[i]][week[j]] = {}
				if time[k] not in section_tt[sections[i]][week[j]]:
					section_tt[sections[i]][week[j]][time[k]] = []
				section_tt[sections[i]][week[j]][time[k]].append(new1[j][k])
			
	return section_tt		

def teacher_time(sec_dict,section_tt,week,time):
	teach_time = []
	
	for d in sec_dict:
		temp = []
		for i in week:
			j = 0
			tmp = []
			while j < len(time):
				x = " "
				for t in sec_dict[d]:
					x = ' '.join(map(str,section_tt[d][i][time[j]]))
					if x in sec_dict[d][t]:
						tmp.append(t)
				j += 1
			temp.append(tmp)
		teach_time.append(temp)
	return teach_time



def subj_allot(sections,n_subjects,teach_hours):
	sec_dict = {}
	sub_for_teachers = 0
	n_of_teachers = 0
	for i in range(len(sections)):
		k = 1
		sec_dict[sections[i]] = {}
		while k <= n_subjects:
			if sub_for_teachers == 0:
				n_of_teachers += 1
			sec_dict[sections[i]]['T'+str(n_of_teachers)]=[]
       
			while sub_for_teachers < teach_hours and k <= n_subjects:
				sec_dict[sections[i]]['T'+str(n_of_teachers)].append('sub'+str(k%6))
				sub_for_teachers += 1
				k += 1
			if sub_for_teachers >= teach_hours:
				sub_for_teachers=0
	return sec_dict			




	
new1 = sub(week,time,n_subjects)
x= section_time(new1,sections,week,time)
'''
for i in x:
	print(i)
	for j in week:
		print(j)
		print(x[i][j])
'''
sec_dict = subj_allot(sections,n_subjects,teach_hours)
y = teacher_time(sec_dict,x,week,time)		
		
for i in range(len(y)):
	print()
	print("\t","Section:",sections[i])
	print()
	print("  8-9am","9-10am","11-12am","12-1pm")
	d=0
	for k in y[i]:
		print()
		print(week[d],k)
		d+=1
print()		