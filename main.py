import last as l

sections = ["a","b","c","d","e"]
week = ["Mon","Tue","Wed","Thu","Fri"]
time= ["8-9","9-10","11-12","12-1"]
n_subjects =5
teach_hours = 3

def prin_sec_table():
	new1 = l.sub(week,time,n_subjects)
	x= l.section_time(new1,sections,week,time)

	for i in x:
		print()
		print("section:",i)
		for j in week:
			print(j)
			print(x[i][j])
		print()	
			
#prin_sec_table()			

def prin_teach_table():
	new1 = l.sub(week,time,n_subjects)
	sec_dict = l.subj_allot(sections,n_subjects,teach_hours)
	x = l.section_time(new1,sections,week,time)
	y = l.teacher_time(sec_dict,x,week,time)		
		
	for i in range(len(y)):
		print()
		print("\t","Section:",sections[i])
		print()
		print("  8-9am","9-10am","11-12am","12-1pm")
		d=0
		for k in y[i]:
			print()
			print(week[d],k)
			d+=1
	print()		
	
prin_teach_table()	